// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class SpinTalonFX extends SubsystemBase {
  /** Creates a new SpinTalonFX. */

  private TalonFX tfx;

  public SpinTalonFX() {
    tfx = new TalonFX(Constants.TALON_FX_PORT);
  }

  public void enable() {
    tfx.set(ControlMode.PercentOutput, 0.5);
  }

  public void disable() {
    tfx.set(ControlMode.PercentOutput, 0.0);
  }

}
